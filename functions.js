var lang = document.documentElement.lang,
    lastScrollPos = 0,
    valid = true,
    blogBtn = document.getElementById('blogBtn'),
    productsSlider = document.getElementById('productsSlider'),
    reviewForm = document.getElementById('reviewForm'),
    reviewSubmit = document.getElementById('reviewSubmit'),
    reviewResponse = document.getElementById('reviewResponse'),
    commentsSlider = document.getElementById('commentsSlider');

/* CLASS FOR FORM */
const formClass = {
    validateInput: (input) => {
        switch (input.name) {
            case 'name':
                if (input.value.length < 2) {
                    input.nextElementSibling.classList.remove('hidden');
                    return false;
                } else {
                    input.nextElementSibling.classList.add('hidden');
                }
                break;

            case 'product':
                if (input.value == '') {
                    input.nextElementSibling.classList.remove('hidden');
                    return false;
                } else {
                    input.nextElementSibling.classList.add('hidden');
                }
                break;

            case 'email':
                if (input.value.length < 2) {
                    input.nextElementSibling.classList.remove('hidden');
                    return false;
                } else {
                    if (formClass.validateEmail(input.value) == false) {
                        input.nextElementSibling.classList.remove('hidden');
                        return false;
                    } else {
                        input.nextElementSibling.classList.add('hidden');
                    }
                }
                break;

            case 'review':
                if (input.value.length < 2) {
                    input.nextElementSibling.classList.remove('hidden');
                    return false;
                } else {
                    input.nextElementSibling.classList.add('hidden');
                }
                break;

            default:
                return true;
                break;
        }
    },
    validateEmail(email) {
        'use strict';
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(email);
    },
    submitUser: () => {
        var formData = new FormData(document.getElementById('reviewForm'));
        formData.append('action', "custom_register_review");
        reviewResponse.innerHTML = '<div class="custom-loader"><div class="lds-dual-ring"></div></div>';

        var xhr = new XMLHttpRequest();
        xhr.open("POST", axim_ajax_object.ajaxurl, true);
        xhr.onload = function(e) {
            var result = JSON.parse(xhr.responseText);
            reviewResponse.innerHTML = result.data.response;
            setTimeout(() => {
                window.location.replace(document.getElementById('reviewForm').getAttribute('data-redirect'));
            }, 500);
        };
        xhr.onerror = function() {
            return false;
        };
        xhr.send(formData);
    }
};

/* CUSTOM ON LOAD FUNCTIONS */
function aximCustomLoad() {
    "use strict";
    console.log('Functions Correctly Loaded');

    document.addEventListener('mouseover', (e) => {
        if (e.target && e.target.classList.contains('select-star') == true) {
            var pos = e.target.getAttribute('data-rating');
            for (let index = 1; index <= pos; index++) {
                document.querySelector('.star-' + index).classList.add('selected-star');
            }
        }
    });

    document.addEventListener('mouseout', (e) => {
        if (e.target && e.target.classList.contains('select-star') == true) {
            var pos = e.target.getAttribute('data-rating');

            for (let index = 1; index <= pos; index++) {
                document.querySelector('.star-' + index).classList.remove('selected-star');
            }
        }
    });

    document.addEventListener('click', (e) => {
        if (e.target && e.target.classList.contains('select-star') == true) {
            for (let index = 1; index <= 5; index++) {
                document.querySelector('.star-' + index).classList.remove('fixed-star');
            }
            var pos = e.target.getAttribute('data-rating');
            document.getElementById('rating').value = pos;
            for (let index = 1; index <= pos; index++) {
                document.querySelector('.star-' + index).classList.add('fixed-star');
            }
        }
    });


    if (blogBtn) {
        if (lang == 'en-US') {
            var link = blogBtn.getAttribute('data-en');
        } else {
            var link = blogBtn.getAttribute('data-es');
        }
        document.querySelector('#blogBtn a').setAttribute('href', decodeURI(link));
    }

    if (reviewForm) {
        document.addEventListener('click', (e) => {
            if (e.target && e.target.id == 'reviewSubmit') {
                var formInputs = document.getElementsByClassName('custom-review-item')
                e.preventDefault();
                valid = true;
                for (let index = 0; index < formInputs.length; index++) {
                    const element = formInputs[index];
                    var value = formClass.validateInput(element);
                    if (value == false) {
                        valid = false;
                    }
                }

                if (valid == true) {
                    formClass.submitUser();
                }

                for (let index = 0; index < formInputs.length; index++) {
                    valid = true;
                    const element = formInputs[index];
                    element.addEventListener('focusout', (e) => {
                        valid = formClass.validateInput(element);
                    });
                }
            }
        });


    }

    if (productsSlider) {
        const swiper = new Swiper('.swiper-productos', {
            loop: true,
            autoplay: {
                delay: 5000,
            },
            pagination: {
                el: '.swiper-productos-pagination',
                clickable: true
            },
            navigation: {
                nextEl: '.swiper-productos-button-next',
                prevEl: '.swiper-productos-button-prev',
            },
            breakpoints: {
                0: {
                    spaceBetween: 15,
                    slidesPerView: 1
                },
                600: {
                    spaceBetween: 15,
                    slidesPerView: 2
                },
                768: {
                    spaceBetween: 15,
                    slidesPerView: 2
                },
                1024: {
                    spaceBetween: 20,
                    slidesPerView: 3
                }
            }
        });
    }

    if (commentsSlider) {
        const swiper2 = new Swiper('.swiper-comments', {
            loop: true,
            autoplay: {
                delay: 5000,
            },
            pagination: {
                el: '.swiper-comments-pagination',
                clickable: true
            },
            navigation: {
                nextEl: '.swiper-comments-button-next',
                prevEl: '.swiper-comments-button-prev',
            },
            breakpoints: {
                0: {
                    spaceBetween: 15,
                    slidesPerView: 1
                },
                600: {
                    spaceBetween: 15,
                    slidesPerView: 2
                },
                768: {
                    spaceBetween: 15,
                    slidesPerView: 2
                },
                1024: {
                    spaceBetween: 20,
                    slidesPerView: 3
                }
            }
        });
    }

    window.addEventListener("scroll", function() {
        var distanceScrolled = document.documentElement.scrollTop;
        if (distanceScrolled > 100) {
            if (distanceScrolled < lastScrollPos) {
                document.getElementById('mainHeader').classList.remove('hidden-header');
            } else {
                document.getElementById('mainHeader').classList.add('hidden-header');
            }
            lastScrollPos = distanceScrolled <= 0 ? 0 : distanceScrolled; // For Mobile or negative scrolling
        } else {
            document.getElementById('mainHeader').classList.add('hidden-header');
        }
    });
}

document.addEventListener("DOMContentLoaded", aximCustomLoad, false);