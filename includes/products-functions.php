<?php

/**
 * Method custom_cpt_products
 *
 * @return void
 */
function custom_cpt_products()
{
    $labels = array(
        'name'                  => _x('Productos', 'Post Type General Name', 'axim'),
        'singular_name'         => _x('Producto', 'Post Type Singular Name', 'axim'),
        'menu_name'             => __('Productos', 'axim'),
        'name_admin_bar'        => __('Productos', 'axim'),
        'archives'              => __('Archivo de Productos', 'axim'),
        'attributes'            => __('Atributos de Producto', 'axim'),
        'parent_item_colon'     => __('Producto Padre:', 'axim'),
        'all_items'             => __('Todos los Productos', 'axim'),
        'add_new_item'          => __('Agregar Nuevo Producto', 'axim'),
        'add_new'               => __('Agregar Nuevo', 'axim'),
        'new_item'              => __('Nuevo Producto', 'axim'),
        'edit_item'             => __('Editar Producto', 'axim'),
        'update_item'           => __('Actualizar Producto', 'axim'),
        'view_item'             => __('Ver Producto', 'axim'),
        'view_items'            => __('Ver Productos', 'axim'),
        'search_items'          => __('Buscar Producto', 'axim'),
        'not_found'             => __('No hay resultados', 'axim'),
        'not_found_in_trash'    => __('No hay resultados en Papelera', 'axim'),
        'featured_image'        => __('Imagen del Producto', 'axim'),
        'set_featured_image'    => __('Colocar Imagen del Producto', 'axim'),
        'remove_featured_image' => __('Remover Imagen del Producto', 'axim'),
        'use_featured_image'    => __('Usar como Imagen del Producto', 'axim'),
        'insert_into_item'      => __('Insertar en Producto', 'axim'),
        'uploaded_to_this_item' => __('Cargado a este Producto', 'axim'),
        'items_list'            => __('Listado de Productos', 'axim'),
        'items_list_navigation' => __('Nav. del Listado de Productos', 'axim'),
        'filter_items_list'     => __('Filtro del Listado de Productos', 'axim'),
    );
    $args = array(
        'label'                 => __('Producto', 'axim'),
        'description'           => __('Productos', 'axim'),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'comments' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-products',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'show_in_rest'          => true,
    );
    register_post_type('productos', $args);
}

add_action('init', 'custom_cpt_products', 0);

/**
 * Method register_products_metabox
 * Register meta box(es).
 *
 * @return void
 */
function register_products_metabox()
{
    add_meta_box('products_metabox', __('Extra information', 'axim'), 'products_metabox_callback', 'productos');
}

add_action('add_meta_boxes', 'register_products_metabox');

/**
 * Method products_metabox_callback
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 *
 * @return void
 */
function products_metabox_callback($post)
{
    wp_nonce_field('products_metabox', 'products_metabox_nonce');
    // PRODUCT SUBTITLE
    $value = get_post_meta($post->ID, 'product_subtitle', true);
    ?>
<label for="product_subtitle">
    <?php _e('Product Subtitle', 'axim'); ?>
</label>
<input type="text" name="product_subtitle" id="product_subtitle" value="<?php echo $value; ?>" />
<?php
    // PRODUCT COMPOSITION
    $value = get_post_meta($post->ID, 'product_composition', true);
    ?>
<label for="product_composition">
    <?php _e('Product Composition', 'axim'); ?>
</label>
<?php wp_editor($value, 'product_composition', $settings = array('media_buttons' => false, 'textarea_name'=>'productComposition', 'textarea_rows' => get_option('default_post_edit_rows', 4)));
    // PRODUCT PRESENTATION
    $value = get_post_meta($post->ID, 'product_presentation', true);
    ?>
<label for="product_presentation">
    <?php _e('Product Presentation', 'axim'); ?>
</label>
<?php wp_editor($value, 'product_presentation', $settings = array('media_buttons' => false, 'textarea_name'=>'productPresentation', 'textarea_rows' => get_option('default_post_edit_rows', 4)));
}

/**
 * Method save_products_metabox
 * Save meta box content.
 *
 * @param int $post_id Post ID
 *
 * @return void
 */
function save_products_metabox($post_id)
{
    if (! isset($_POST['products_metabox_nonce'])) {
        return $post_id;
    }

    $nonce = $_POST['products_metabox_nonce'];

    // CHECK NONCE
    if (! wp_verify_nonce($nonce, 'products_metabox')) {
        return $post_id;
    }

    // CHECK AUTOSAVE
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    $mydata = $_POST['product_subtitle'];
    update_post_meta($post_id, 'product_subtitle', $mydata);

    $mydata = $_POST['productComposition'];
    update_post_meta($post_id, 'product_composition', $mydata);

    $mydata = $_POST['productPresentation'];
    update_post_meta($post_id, 'product_presentation', $mydata);
}
add_action('save_post', 'save_products_metabox');

/**
 * Method custom_slider_call
 *
 * @param $atts $atts [explicite description]
 * @param $content $content [explicite description]
 *
 * @return void
 */
function custom_slider_call($atts, $content = null)
{
    $values = shortcode_atts(array(), $atts);
    ob_start();
    $current_lang = get_locale();
    $lang = ($current_lang == 'es_ES') ? 'es' : 'en';
    $args = array('post_type' => 'productos', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date', 'lang' => $lang);
    $arr_products = get_posts($args);
    if ($arr_products) :
        ?>
<div class="swiper-custom-wrapper">
    <div id="productsSlider" class="swiper swiper-productos">
        <div class="swiper-wrapper">
            <?php
                foreach ($arr_products as $item) :
                    $post_id = $item->ID;
                    ?>
            <div class="swiper-slide">
                <article id="<?php echo $post_id; ?>" class="product-item">
                    <div class="products-item-wrapper">
                        <picture class="product-image">
                            <?php echo get_the_post_thumbnail($post_id, 'products_thumb', array('class' => 'img-fluid')); ?>
                        </picture>
                        <header class="product-title">
                            <h2><?php echo  $item->post_title; ?></h2>
                            <div class="product-reviews">
                                <?php $promedio = get_custom_product_rating($post_id); ?>
                                <span class="rating-number"><?php echo number_format($promedio, 1, '.', ''); ?></span>
                                <?php $rating = round($promedio); ?>
                                <?php for ($i=0; $i < $rating; $i++) { ?>
                                <i class="fas fa-star"></i>
                                <?php } ?>
                                <?php if ($rating < 5) { ?>
                                <?php for ($y = $rating; $y < 5; $y++) { ?>
                                <i class="fas fa-star dark-star"></i>
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <h3><?php echo get_post_meta($post_id, 'product_subtitle', true); ?></h3>
                        </header>
                        <div class="custom-product-info">
                            <div class="product-content">
                                <?php echo apply_filters('the_content', $item->post_content); ?>
                            </div>
                            <div class="product-composition">
                                <h4><?php _e('Composición', 'axim'); ?>:</h4>
                                <?php echo apply_filters('the_content', get_post_meta($post_id, 'product_composition', true)); ?>
                            </div>
                            <div class="product-presentation">
                                <h4><?php _e('Presentación(es)', 'axim'); ?>:</h4>
                                <?php echo apply_filters('the_content', get_post_meta($post_id, 'product_presentation', true)); ?>
                            </div>
                        </div>
                        <div class="product-button">
                            <?php if ($lang == 'es') { ?>
                            <a href="#elementor-action%3Aaction%3Dpopup%3Aopen%26settings%3DeyJpZCI6IjM4MyIsInRvZ2dsZSI6ZmFsc2V9"><?php _e('Comprar en', 'axim'); ?> <img src="<?php echo get_stylesheet_directory_uri(); ?>/amazon-logo-white.svg" alt="Amazon"></a>
                            <?php } else { ?>
                            <a href="#elementor-action%3Aaction%3Dpopup%3Aopen%26settings%3DeyJpZCI6IjM3OSIsInRvZ2dsZSI6ZmFsc2V9"><?php _e('Comprar en', 'axim'); ?> <img src="<?php echo get_stylesheet_directory_uri(); ?>/amazon-logo-white.svg" alt="Amazon"></a>
                            <?php } ?>
                        </div>
                    </div>
                </article>
            </div>
            <?php
                endforeach;
    wp_reset_postdata();
    ?>
        </div>
    </div>
    <div class="swiper-pagination swiper-productos-pagination"></div>
    <div class="swiper-button-prev swiper-productos-button-prev"></div>
    <div class="swiper-button-next swiper-productos-button-next"></div>
</div>
<?php
    endif;
    $content = ob_get_clean();
    return $content;
}

add_shortcode('custom_slider', 'custom_slider_call');

/**
 * Method get_custom_product_rating
 *
 * @param $post_id $post_id [explicite description]
 *
 * @return void
 */
function get_custom_product_rating($post_id)
{
    $acum = 0;
    $quantity = 0;

    $current_comments[] = get_approved_comments($post_id);
    if (!empty($current_comments[0])) {
        foreach ($current_comments[0] as $item) {
            $rating = get_comment_meta($item->comment_ID, 'rating', true);
            $acum = $acum + $rating;
            $quantity++;
        }
        $promedio = $acum / $quantity;
    } else {
        $promedio = 0;
    }

    return $promedio;
}
