<?php

/**
 * Method custom_breadcrumbs_call
 *
 * @param $atts $atts [explicite description]
 * @param $content $content [explicite description]
 *
 * @return void
 */
function custom_breadcrumbs_call($atts, $content = null)
{
    $values = shortcode_atts(array(), $atts);
    ob_start();
    $page_id = get_queried_object_id();
    ?>
<ol class="custom-breadcrumbs">
    <li><a href="<?php echo home_url('/'); ?>"><?php _e('Inicio', 'axim'); ?></a></li>
    <?php if (is_category()) { ?>
    <?php $cat = get_the_category_by_ID($page_id); ?>
    <li><a href="<?php echo get_permalink($page_id); ?>"><?php echo $cat; ?></a></li>
    <?php } else { ?>
    <li><a href="<?php echo get_permalink($page_id); ?>"><?php echo get_the_title($page_id); ?></a></li>
    <?php } ?>
</ol>
<?php
    $content = ob_get_clean();
    return $content;
}

add_shortcode('custom_breadcrumbs', 'custom_breadcrumbs_call');

/**
 * Method custom_categories_dropdown_call
 *
 * @param $atts $atts [explicite description]
 * @param $content $content [explicite description]
 *
 * @return void
 */
function custom_categories_dropdown_call($atts, $content = null)
{
    $values = shortcode_atts(array(), $atts);
    ob_start();
    $page_id = get_queried_object_id();
    ?>
<div class="custom-categories-dropdown">
    <span><?php _e('Categorias', 'axim'); ?></span>
    <div class="custom-dropdown">
        <span><?php _e('Todas las publicaciones', 'axim'); ?></span>
        <img src="<?php echo get_stylesheet_directory_uri() ?>/icon.svg" class="img-chevron" />
        <ul class="dropdown">
            <?php $categories = get_categories(array( 'orderby' => 'name', 'parent'  => 0 )); ?>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo esc_url(get_category_link($category->term_id)); ?>"><?php echo esc_html($category->name); ?></a></li>
            <?php } ?>
        </ul>
    </div>
</div>
<?php
    $content = ob_get_clean();
    return $content;
}

add_shortcode('custom_categories_dropdown', 'custom_categories_dropdown_call');
