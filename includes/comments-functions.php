<?php

/**
 * Method custom_comments_submit_callback
 *
 * @param $atts $atts [explicite description]
 * @param $content $content [explicite description]
 *
 * @return void
 */
function custom_comments_submit_callback($atts, $content = null)
{
    $values = shortcode_atts(array(
        'url_redirect' => '#'
    ), $atts);
    ob_start();
    $current_lang = get_locale();
    $lang = ($current_lang == 'es_ES') ? 'es' : 'en';
    $args = array('post_type' => 'productos', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date', 'lang' => $lang);
    $arr_products = get_posts($args);
    if ($arr_products) :
        foreach ($arr_products as $item) :
            $products_select[$item->ID] = $item->post_title;
        endforeach;
        wp_reset_postdata();
    endif;
    ?>
<form id="reviewForm" class="custom-form-container" data-redirect="<?php echo $values['url_redirect']; ?>">
    <div class="custom-form-container">
        <label for="product"><?php _e('Producto', 'axim'); ?></label>
        <select name="product" id="product" class="custom-review-item review-item review-item-select">
            <option value="" disabled selected><?php _e('Seleccciona un Producto', 'axim'); ?></option>
            <?php foreach ($products_select as $key => $value) { ?>
            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
            <?php } ?>
        </select>
        <small class="danger hidden"><?php _e('Debe seleccionar un producto', 'axim'); ?></small>
    </div>
	<div class="custom-form-container">
        <label for="name"><?php _e('Calificación', 'axim'); ?></label>
		<div class="rating-container">
			<i class="select-star star-1 fas fa-star" data-rating="1"></i>
			<i class="select-star star-2 fas fa-star" data-rating="2"></i>
			<i class="select-star star-3 fas fa-star" data-rating="3"></i>
			<i class="select-star star-4 fas fa-star" data-rating="4"></i>
			<i class="select-star star-5 fas fa-star" data-rating="5"></i>
		</div>
        <input type="hidden" name="rating" id="rating" class="custom-review-item review-item">
        <small class="danger hidden"><?php _e('Debe seleccionar su calificación', 'axim'); ?></small>
    </div>
    <div class="custom-form-container">
        <label for="name"><?php _e('Nombre', 'axim'); ?></label>
        <input type="text" name="name" id="name" class="custom-review-item review-item">
        <small class="danger hidden"><?php _e('Debe ingresar su nombre', 'axim'); ?></small>
    </div>
    <div class="custom-form-container">
        <label for="email"><?php _e('Correo Electrónico', 'axim'); ?></label>
        <input type="email" name="email" id="email" class="custom-review-item review-item">
        <small class="danger hidden"><?php _e('Debe ingresar su correo electrónico', 'axim'); ?></small>
    </div>
    <div class="custom-form-container">
        <label for="email"><?php _e('Comentarios', 'axim'); ?></label>
        <textarea name="review" id="review" cols="30" rows="5" class="custom-review-item review-item review-item-textarea"></textarea>
        <small class="danger hidden"><?php _e('Debe ingresar su comentario', 'axim'); ?></small>
    </div>
    <div class="custom-form-container">
        <button type="submit" id="reviewSubmit" class="review-btn"><?php _e('Enviar testimonio', 'axim'); ?></button>
        <div id="reviewResponse" class="response"></div>
    </div>
</form>
<?php
    $content = ob_get_clean();
    return $content;
}

add_shortcode('custom_comments_form', 'custom_comments_submit_callback');


/**
 * Method custom_register_review_callback
 *
 * @return void
 */
function custom_register_review_callback()
{
    if (defined('WP_DEBUG') && WP_DEBUG) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }

    $posted_data =  isset($_POST) ? $_POST : array();

    $data_info = array(
        'comment_post_ID'      => $posted_data['product'],
        'comment_content'      => $posted_data['review'],
        'comment_approved'     => 0,
        'user_id'              => 0,
        'comment_author'       => $posted_data['name'],
        'comment_author_email' => $posted_data['email'],
        'comment_meta'         => array(
            'custom_name'       => $posted_data['name'],
            'rating'            => $posted_data['rating']
        )
    );

    $comment_id = wp_insert_comment($data_info);



    $data = array(
        'response' => __('Comentario Registrado', 'axim'),
        'status' => 'success',
        'comment_id' => $comment_id
    );

    wp_send_json_success($data, 200);
    wp_die();
}

add_action('wp_ajax_custom_register_review', 'custom_register_review_callback');
add_action('wp_ajax_nopriv_custom_register_review', 'custom_register_review_callback');


/**
 * Method custom_comments_slider_call
 *
 * @param $atts $atts [explicite description]
 * @param $content $content [explicite description]
 *
 * @return void
 */
function custom_comments_slider_call($atts, $content = null)
{
    $values = shortcode_atts(array(), $atts);
    ob_start();
    $current_lang = get_locale();
    $current_comments = array();
    $lang = ($current_lang == 'es_ES') ? 'es' : 'en';
    $args = array('post_type' => 'productos', 'posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date', 'lang' => $lang);
    $arr_products = get_posts($args);
    if ($arr_products) :
        foreach ($arr_products as $item) :
            $current_comments[] = get_approved_comments($item->ID);
        endforeach;
        wp_reset_postdata();
    endif;
    ?>
<div class="swiper-custom-wrapper">
    <div id="commentsSlider" class="swiper swiper-comments">
        <div class="swiper-wrapper">
            <?php
            $comment_list = array();
    foreach ($current_comments as $comment) {
        foreach ($comment as $item) {
            if (!in_array($item->comment_ID, $comment_list)) {
                ?>
            <div class="swiper-slide">
                <article class="comments-item">
                    <div class="comments-item-wrapper">
                        <h3><?php echo get_comment_meta($item->comment_ID, 'custom_name', true); ?></h3>
                        <div class="comments-rating">
                            <?php $rating = get_comment_meta($item->comment_ID, 'rating', true); ?>
                            <?php for ($i=0; $i < $rating; $i++) { ?>
                            <i class="fas fa-star"></i>
                            <?php } ?>
							<?php if ($rating < 5) { ?>
								<?php for ($y = $rating; $y < 5; $y++) { ?>
								<i class="fas fa-star dark-star"></i>
								<?php } ?>
							<?php } ?>
                        </div>
                        <h4><?php echo get_the_title($item->comment_post_ID); ?></h4>
                        <div class="comments-content">
                            <?php echo apply_filters('the_content', $item->comment_content); ?>
                        </div>
                    </div>
                </article>
            </div>
            <?php
                $comment_list[] = $item->comment_ID;
            }
        }
    }
    ?>
        </div>
    </div>
    <div class="swiper-pagination swiper-comments-pagination"></div>
    <div class="swiper-button-prev swiper-comments-button-prev"></div>
    <div class="swiper-button-next swiper-comments-button-next"></div>
</div>
<?php

    $content = ob_get_clean();
    return $content;
}

add_shortcode('custom_comments_slider', 'custom_comments_slider_call');


/**
 * Method extend_comment_add_meta_box
 *
 * @return void
 */

function extend_comment_add_meta_box()
{
    add_meta_box('title', __('Comment Metadata - Extend Comment'), 'extend_comment_meta_box', 'comment', 'normal', 'high');
}

add_action('add_meta_boxes_comment', 'extend_comment_add_meta_box');

/**
 * Method extend_comment_meta_box
 *
 * @param $comment $comment [explicite description]
 *
 * @return void
 */
function extend_comment_meta_box($comment)
{
    $custom_name = get_comment_meta($comment->comment_ID, 'custom_name', true);
    $rating = get_comment_meta($comment->comment_ID, 'rating', true);

    wp_nonce_field('extend_comment_update', 'extend_comment_update', false);
    ?>
<p>
    <label for="custom_name"><?php _e('Name'); ?></label>
    <input type="text" name="custom_name" value="<?php echo esc_attr($custom_name); ?>" class="widefat" />
</p>
<p>
    <label for="rating"><?php _e('Rating: '); ?></label>
    <span class="commentratingbox">
        <?php for($i=1; $i <= 5; $i++) {
            echo '<span class="commentrating"><input type="radio" name="rating" id="rating" value="'. $i .'"';
            if ($rating == $i) {
                echo ' checked="checked"';
            }
            echo ' />'. $i .' </span>';
        }
    ?>
    </span>
</p>
<?php
}


/**
 * Method save_comment_meta_data
 *
 * @param $comment_id $comment_id [explicite description]
 *
 * @return void
 */
function save_comment_meta_data($comment_id)
{
    if ((isset($_POST['custom_name'])) && ($_POST['custom_name'] != ’)) {
        $title = wp_filter_nohtml_kses($_POST['custom_name']);
    }
    add_comment_meta($comment_id, 'custom_name', $title);

    if ((isset($_POST['rating'])) && ($_POST['rating'] != ’)) {
        $rating = wp_filter_nohtml_kses($_POST['rating']);
    }
    add_comment_meta($comment_id, 'rating', $rating);
}

add_action('comment_post', 'save_comment_meta_data');

/**
 * Method extend_comment_edit_metafields
 *
 * @param $comment_id $comment_id [explicite description]
 *
 * @return void
 */
function extend_comment_edit_metafields($comment_id)
{
    if(! isset($_POST['extend_comment_update']) || ! wp_verify_nonce($_POST['extend_comment_update'], 'extend_comment_update')) {
        return;
    }

    if ((isset($_POST['custom_name'])) && ($_POST['custom_name'] != ’)):
        $title = wp_filter_nohtml_kses($_POST['custom_name']);
        update_comment_meta($comment_id, 'custom_name', $title);
    else :
        delete_comment_meta($comment_id, 'custom_name');
    endif;

    if ((isset($_POST['rating'])) && ($_POST['rating'] != ’)):
        $rating = wp_filter_nohtml_kses($_POST['rating']);
        update_comment_meta($comment_id, 'rating', $rating);
    else :
        delete_comment_meta($comment_id, 'rating');
    endif;
}

add_action('edit_comment', 'extend_comment_edit_metafields');

/**
 * Method rudr_add_comments_columns
 *
 * @param $my_cols $my_cols [explicite description]
 *
 * @return void
 */
function rudr_add_comments_columns($my_cols)
{
    $misha_columns = array(
        'custom_name' => 'Name of Review',
        'rating' => 'Rating'
    );
    $my_cols = array_slice($my_cols, 0, 3, true) + $misha_columns + array_slice($my_cols, 3, null, true);

    return $my_cols;
}

add_filter('manage_edit-comments_columns', 'rudr_add_comments_columns');

/**
 * Method rudr_add_comment_columns_content
 *
 * @param $column $column [explicite description]
 * @param $comment_ID $comment_ID [explicite description]
 *
 * @return void
 */
function rudr_add_comment_columns_content($column, $comment_ID)
{
    global $comment;
    switch ($column) :
        case 'custom_name': {
            echo get_comment_meta($comment_ID, 'custom_name', true);
            break;
        }
        case 'rating': {
            $rating = get_comment_meta($comment_ID, 'rating', true);
            for ($i=0; $i < $rating; $i++) { ?>
<span class="dashicons dashicons-star-filled"></span>
<?php
            }
            break;
        }
    endswitch;
}

add_action('manage_comments_custom_column', 'rudr_add_comment_columns_content', 10, 2);
