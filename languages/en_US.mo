��    ;      �  O   �           	          +     B     W     m     z     �     �  
   �     �     �     �  !   �       
   (     3     G  $   c     �  !   �     �     �     �     �          "  T   B     �     �     �     �     �     �     �          #     A     H      W      x     �     �     �     �     �     �  	   �     	     	     -	     E	     ]	     q	     �	     �	     �	  )   �	  �  �	     �     �     �     �               %     4     ;  
   T     _     s     �  !   �     �     �     �     �     �     �          3     M     Z     i     w     �  T   �     �                    ,     <     A  
   _     j     ~     �     �     �     �     �     �     �     �     �                    )  	   :     D     Q     f     s     �  )   �           *   4   3      	      "   5      )   6   !                    ,            ;                            $      (              
   '             &      .                     2       /   8                  :      %   7   1   9       #      -   +                               0    Actualizar Producto Agregar Nuevo Agregar Nuevo Producto Archivo de Productos Atributos de Producto Axim Landing Buscar Producto Calificación Cargado a este Producto Categorias Colocar Imagen del Producto Comentario Registrado Comentarios Comment Metadata - Extend Comment Composición Comprar en Correo Electrónico Debe ingresar su comentario Debe ingresar su correo electrónico Debe ingresar su nombre Debe seleccionar su calificación Debe seleccionar un producto Editar Producto Elementor Team Enviar testimonio Extra information Filtro del Listado de Productos Hello Elementor Child is a child theme of Hello Elementor, created by Elementor team Imagen del Producto Inicio Insertar en Producto Language Switcher Listado de Productos Name Nav. del Listado de Productos No hay resultados No hay resultados en Papelera Nombre Nuevo Producto Post Type General NameProductos Post Type Singular NameProducto Presentación(es) Product Composition Product Presentation Product Subtitle Producto Producto Padre: Productos Rating:  Remover Imagen del Producto Seleccciona un Producto Todas las publicaciones Todos los Productos Usar como Imagen del Producto Ver Producto Ver Productos https://elementor.com/ https://github.com/elementor/hello-theme/ Project-Id-Version: Axim Theme
PO-Revision-Date: 2022-09-16 15:39-0400
Last-Translator: 
Language-Team: Robert Ochoa <ochoa.robert1@gmail.com>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: languages
X-Poedit-SearchPath-1: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Update Product Add New Add New Product Product Archive Product Attributes Axim Landing Search Product Rating Uploaded to this Product Categories Place Product Image Comment Saved Review Comment Metadata - Extend Comment Composition Available at Email You must enter your review You must enter your email You must enter your name You must select your rating You must select a product Edit Product Elementor Team Submit Review Extra information Product Listing Filter Hello Elementor Child is a child theme of Hello Elementor, created by Elementor team Product Image Home Insert in Product Language Switcher Product Listing Name Nav. from the Product Listing No results No results in Trash Name New Product Products Product Presentation(s) Product Composition Product Presentation Product Subtitle Product Parent Product: Products Rating:  Remove Product Image Select a Product All Posts All Products Use as Product Image View Product View Products https://elementor.com/ https://github.com/elementor/hello-theme/ 