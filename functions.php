<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

/**
  * ADD COMMENTS FUNCTIONS
  *
  * @package HelloElementorChild
*/

require_once('includes/comments-functions.php');

/**
  * ADD PRODUCTS FUNCTIONS
  *
  * @package HelloElementorChild
*/
require_once('includes/products-functions.php');

/**
  * ADD CUSTOM SHORTCODES
  *
  * @package HelloElementorChild
*/
require_once('includes/custom-shortcodes.php');

/**
 * Method hello_elementor_child_enqueue_scripts
 * Load child theme css and optional scripts
 *
 * @return void
 */
function hello_elementor_child_enqueue_scripts()
{
    // CHILD THEME STYLE
    wp_enqueue_style(
        'hello-elementor-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        [
            'hello-elementor-theme-style',
        ],
        '1.0.0'
    );
    // SWIPER.JS STYLE
    wp_enqueue_style(
        'swiperjs-css',
        'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css',
        [
            'hello-elementor-theme-style',
        ],
        '1.0.0'
    );
    // SWIPER.JS SCRIPT
    wp_enqueue_script(
        'swiperjs',
        'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js',
        [],
        '1.0.0',
        true
    );
    // MAIN SCRIPT
    wp_enqueue_script(
        'hello-elementor-child-functions',
        get_stylesheet_directory_uri() . '/functions.js',
        ['swiperjs'],
        '1.0.0',
        true
    );
    // WP LOCALIZE CHILD SCRIPT
    wp_localize_script(
        'hello-elementor-child-functions',
        'axim_ajax_object',
        array(
            'ajaxurl' => admin_url('admin-ajax.php')
        )
    );
}
add_action('wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts', 20);

/**
  * ADD CUSTOM IMAGE SIZE
  *
  * @package HelloElementorChild
*/
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
}
if (function_exists('add_image_size')) {
    add_image_size('products_thumb', 300, 300, false);
}

/**
 * Method custom_language_translate
 *
 * @return void
 */
function custom_language_translate()
{
    load_child_theme_textdomain('axim', get_stylesheet_directory() . '/languages');
}

add_action('after_setup_theme', 'custom_language_translate');
