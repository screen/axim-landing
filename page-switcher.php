<?php
/**
* Template Name: Language Switcher
*
* @package WordPress
* @subpackage axim_landing
* @since Axim Landing 1.0
*/

$geodata = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $_SERVER['REMOTE_ADDR']));

if (!empty($geodata['geoplugin_countryCode'])) {
    if ($geodata['geoplugin_countryCode'] == 'US') {
        header("Location: https://screen.com.ve/clientes/axim/home");
    } else {
        header("Location: https://screen.com.ve/clientes/axim/es/inicio");
    }
} else {
    header("Location: https://screen.com.ve/clientes/axim/es/inicio");
}
